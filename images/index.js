import Home from "./home.jpg";
import Logo from "./logo.png";

import CancelAnyTime from "./cancel_any_time.png";
import DeviceComputer from "./device_computer.png";
import DevicePhone from "./device_phone.png";
import DeviceTV from "./device_tv.png";

export {
  Home,
  Logo,
  CancelAnyTime,
  DeviceComputer,
  DevicePhone,
  DeviceTV,
};
